# Remote DHT22 sensor for Home Assistant

Features:
* reads temperature and humidity from a DHT22 sensor
* publishes the data to Home Assistant via MQTT (so any other software supporting MQTT is supported as well)
* supports Home Assistant's [MQTT discovery](https://www.home-assistant.io/docs/mqtt/discovery/) feature
* runs on Raspberry Pi and other boards supporting [CircuitPython](https://circuitpython.readthedocs.io/en/latest/shared-bindings/support_matrix.html?filter=adafruit&filter=board&filter=pulseio)
* works with ```systemd``` to auto-start on boot
* based on Python 3, [DHT Library](http://docs.circuitpython.org/projects/dht/en/latest/) and [paho Python Client](https://www.eclipse.org/paho/index.php?page=clients/python/index.php)

## Prerequisites

### Software

* [Home Assistant](https://www.home-assistant.io/installation)
* MQTT broker, e. g. the Home Assistant [Mosquitto add-on](https://github.com/home-assistant/addons/blob/master/mosquitto/DOCS.md)
* Home Assistant [MQTT integration](https://www.home-assistant.io/integrations/mqtt/), connected to the MQTT broker
* Python 3

### Wiring

You need to connect the DHT22 sensor to your board, e. g. a Raspberry Pi. There are plenty of tutorials out there, e. g. [Adafruit DHT tutorial](https://learn.adafruit.com/dht/dht-circuitpython-code).

If you board comes with internal pull-up resistors and you connect the sensor to a corresponding pin, the pull-up resistor will be activated by the script. If you are not sure about this, you can always include a pull-up resistor into your wiring as shown by most tutorials.

### Libraries

You need the following Python libraries:

```
sudo pip3 install adafruit-circuitpython-dht paho-mqtt
```

In addition, ```libgpiod2``` is needed on a Raspberry Pi:

```
sudo apt-get install libgpiod2
```

## Configuration

Copy the provided example file ```dht_mqtt_pub_config.example.py``` to ```dht_mqtt_pub_config.py``` and modify it according to your needs:

```
cp dht_mqtt_pub_config.example.py dht_mqtt_pub_config.py
nano dht_mqtt_pub_config.py
```

The entire configuration is done using this file. There is one exception: the logging level. The latter can set in the main script file, if required.

## Usage 

```
python3 dht_mqtt_pub.py
```

## Auto-start

The script can be configured to automatically start on system boot. There are plenty of ways to achieve this. Here, we focus on using ```systemd```. For more details, see this [blog post](https://alexandra-zaharia.github.io/posts/stopping-python-systemd-service-cleanly/) and this [tutorial](https://code.luasoftware.com/tutorials/linux/auto-start-python-script-on-boot-systemd/).

Open the provided unit file ```dht_mqtt_pub.service``` and check the paths and modify them to match your setup:

```
nano dht_mqtt_pub.service
```

When done, create a symlink or copy the file to you systemd configuration and change the ownership:

```
sudo ln -sf dht_mqtt_pub.service /etc/systemd/system/dht_mqtt_pub.service
sudo chown root: dht_mqtt_pub.service
```

Now, reload the system configuration, enable the service to start on boot and initially start the script:

```
sudo systemctl daemon-reload
sudo systemctl enable dht_mqtt_pub.service
sudo systemctl start dht_mqtt_pub.service
```

## License

Copyright Martin Böhmer

Licensed under the Apache License, Version 2.0 (the "License"); you may not use these files except in compliance with the License.
You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and limitations under the License.
