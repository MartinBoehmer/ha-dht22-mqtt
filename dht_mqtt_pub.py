import importlib
import os
import sys
import time
import signal
import logging
import logging.handlers

import adafruit_dht
import board
import digitalio

import json
import paho.mqtt.client as mqtt

##### Constants #####

DEVICE_TEMPERATURE = "temperature"
DEVICE_HUMIDITY = "humidity"
PAYLOAD_AVAILABLE = "ONLINE"
PAYLOAD_NOT_AVAILABLE = "OFFLINE"
MAX_LOOP_DELAY = 15

##### Helpers #####

def scriptBasename():
    return os.path.splitext(os.path.basename(__file__))[0]

##### Logging #####

logger = logging.getLogger(scriptBasename())
logger.setLevel(logging.INFO)
logFormatter = logging.Formatter('%(asctime)s [%(levelname)s] %(message)s')
logFile = logging.handlers.RotatingFileHandler(filename=scriptBasename()+'.log', maxBytes=1048576, backupCount=2)
logFile.setFormatter(logFormatter)
logger.addHandler(logFile)
logConsole = logging.StreamHandler(sys.stdout)
logConsole.setFormatter(logFormatter)
logger.addHandler(logConsole)

##### Settings #####

def initialiseConfig():
    # Default settings
    cfg = {
        'DHT_PIN_ADA' : board.D4, # DHT data pin, here: RPI GPIO 4
        'DHT_READ_DELAY' : 180, # Delay (seconds) between sensor readings
        'DHT_MAX_READ_ATTEMPTS' : 3, # Each reading may comprise multiple attemps to read from the sensor until the reading is skipped/aborted
        'DHT_ATTEMPT_DELAY' : 3.0, # Seconds between read attempts
        'HA_SENSOR_ID' : "DHTsensor1", # ID to create/identify the multi-valued sensor in Home Assistant (HA)
        'HA_SENSOR_ID_TEMPERATURE' : "DHTsensor1T", # ID to create/identify the sensor's temperature component in Home Assistant
        'HA_SENSOR_ID_HUMIDITY' : "DHTsensor1H", # ID to create/identify the sensor's humidity component in Home Assistant
        'HA_SENSOR_DISCOVERY' : True, # If True, send device discovery messages to Home Assistant
        'HA_SENSOR_AVAILABILITY' : True, # If True, send device availability messages to Home Assistant
        'HA_WAIT_AFTER_DISCOVERY' : 7, # Delay (seconds) after sending discovery to let Home Assistant process the discovery message and create the entitites. Set to 0 to disable delay.
        'HA_SENSOR_NAME_TEMPERATURE' : "Temperature", # Name of the temperature component to appear in Home Assistant
        'HA_SENSOR_NAME_HUMIDITY' : "Humidity", # Name of the humidity component to appear in Home Assistant
        'HA_SENSOR_EXPIRE_AFTER' : 1800, # Number of seconds after the sensor’s state expires, if it’s not updated.
        'MQTT_BROKER_HOST' : "mqtt.homeassistant.local",
        'MQTT_BROKER_PORT' : 8883,
        'MQTT_TOPIC_STATE' : "homeassistant/sensor/{0}/state", # Topic to jointly publish temperature and humidity. Placeholder is set to SENSOR_ID
        'MQTT_TOPIC_AVAILABILITY' : "homeassistant/sensor/{0}/availability", # Topic to indicate sensor's availability. Placeholder is set to SENSOR_ID
        'MQTT_TOPIC_CONFIG' : "homeassistant/sensor/{0}/config", # The sensor's temperature and the humidity each have their own config topics for discovery. Placeholder is set to HA_SENSOR_ID_TEMPERATURE and HA_SENSOR_ID_HUMIDITY
        'MQTT_USERNAME': None,
        'MQTT_PASSWORD' : None,
        'MQTT_CA_CERTS': None, # Path to CA certificate file
        'MQTT_QOS' : 1 # QoS used for all publishing (discovery, availability, state)
    }
    # Load external settings, if present. Config file name is script file name with '_config' appended
    config_module_name = scriptBasename() + "_config"
    try:
        config_module = importlib.import_module(config_module_name)
        logger.debug("Loaded external configuration from " + config_module_name)
        cfg_ext = getattr(config_module, 'cfg')
        cfg.update(cfg_ext)
    except ImportError as error:
        print(error.args[0], file=sys.stderr)
        pass
    logger.debug("Configuration: " + str(cfg))
    return cfg

##### Signals #####

sigterm = False

def signalTerminate(sig, frame):
    logger.info("Received SIGTERM")
    global sigterm
    sigterm = True

def signalHangUp(sig, frame):
    logger.info("Received SIGHUP")

signal.signal(signal.SIGTERM, signalTerminate)
signal.signal(signal.SIGHUP, signalHangUp)

##### MQTT #####

def on_connect(client, userdata, flags, rc):
    if rc == 0:
        logger.info("Connected to MQTT broker")
        userdata['CON_ERR_COUNT'] = 0
    else:
        userdata['CON_ERR_COUNT'] += 1
        msg = "Connection to MQTT broker failed! Result code " + str(rc) + ": " + mqtt.connack_string(rc) + " Attempt: " + str(userdata['CON_ERR_COUNT'])
        logger.error(msg)
        if userdata['CON_ERR_COUNT'] >= 3:
            logger.error("Connection to MQTT broker failed too many times. Giving up.")
            raise RuntimeError(msg)

def initialiseMqtt(cfg):
    mqttClient = mqtt.Client(client_id=cfg['HA_SENSOR_ID'] + "_mqtt", userdata={'connerr_count' : 0,}, clean_session=False)
    mqttClient.enable_logger()
    mqttClient.on_connect = on_connect
    mqttClient.tls_set(ca_certs=cfg['MQTT_CA_CERTS'])
    mqttClient.username_pw_set(cfg['MQTT_USERNAME'], cfg['MQTT_PASSWORD'])
    mqttClient.connect(host=cfg['MQTT_BROKER_HOST'], port=cfg['MQTT_BROKER_PORT'])
    #mqttClient.loop_start()
    mqttClient.loop()
    return mqttClient

def terminateMqtt(mqttClient, cfg):
    logger.debug("Terminating MQTT client")
    mqttClient.loop_stop()
    mqttClient.disconnect()

def publishDiscovery(mqttClient, cfg, deviceClass):
    isTempSensor = deviceClass == DEVICE_TEMPERATURE
    name = cfg['HA_SENSOR_NAME_TEMPERATURE'] if isTempSensor else cfg['HA_SENSOR_NAME_HUMIDITY']
    objectId = cfg['HA_SENSOR_ID_TEMPERATURE'] if isTempSensor else cfg['HA_SENSOR_ID_HUMIDITY']
    uoM = "°C" if isTempSensor else "%"
    mqttDiscovery = {
        "device_class": deviceClass,
        "name": name,
        "object_id" : objectId,
        "unique_id" : objectId,
        "state_topic": cfg['MQTT_TOPIC_STATE'].format(cfg['HA_SENSOR_ID']),
        "unit_of_measurement" : uoM,
        "value_template" : "{{ value_json." + deviceClass + "}}",
        "expire_after" : cfg['HA_SENSOR_EXPIRE_AFTER']
    }
    if cfg['HA_SENSOR_AVAILABILITY']:
        mqttDiscovery.update({
                "availability_topic" : cfg['MQTT_TOPIC_AVAILABILITY'].format(cfg['HA_SENSOR_ID']),
                "payload_available" : PAYLOAD_AVAILABLE,
                "payload_not_available" : PAYLOAD_NOT_AVAILABLE
            })
    mqttDiscoveryTopic = cfg['MQTT_TOPIC_CONFIG'].format(objectId)
    mqttResult = mqttClient.publish(mqttDiscoveryTopic, json.dumps(mqttDiscovery), cfg['MQTT_QOS'])
    #mqttResult.wait_for_publish(timeout=5)
    if mqttResult.rc == mqtt.MQTT_ERR_SUCCESS and mqttResult.is_published:
        logger.debug("Published sensor DISCOVERY with message ID " + str(mqttResult.mid) + ": " + str(mqttDiscovery))
    else:
        raise RuntimeError("Failed to publish sensor DISCOVERY: %s", mqttClient.rc)

def publishAvailability(mqttClient, cfg, availability):
    mqttAvailabilityTopic = cfg['MQTT_TOPIC_AVAILABILITY'].format(cfg['HA_SENSOR_ID'])
    mqttResult = mqttClient.publish(mqttAvailabilityTopic, availability, cfg['MQTT_QOS'])
    #mqttResult.wait_for_publish(timeout=5)
    if mqttResult.rc == mqtt.MQTT_ERR_SUCCESS and mqttResult.is_published:
        logger.info("Published sensor AVAILABILITY with message ID " + str(mqttResult.mid) + ": " + availability)
    else:
        logger.error("Failed to publish sensor AVAILABILITY: %s", mqttClient.rc)

def publishSensorState(state, mqttClient, cfg):
    mqttStateTopic = cfg['MQTT_TOPIC_STATE'].format(cfg['HA_SENSOR_ID'])
    mqttResult = mqttClient.publish(mqttStateTopic, json.dumps(state), cfg['MQTT_QOS'])
    if mqttResult.rc == mqtt.MQTT_ERR_SUCCESS and mqttResult.is_published:
        logger.debug("Published sensor STATE with message ID " + str(mqttResult.mid) + ": " + str(state))
    elif mqttResult.rc == mqtt.MQTT_ERR_NO_CONN:
        logger.warning("Failed to publish sensor STATE, because MQTT client is not connected. Attempting to reconnect.")
        mqttClient.reconnect()
        if cfg['HA_SENSOR_AVAILABILITY']:
            publishAvailability(mqttClient, cfg, PAYLOAD_AVAILABLE)

    else:
        logger.error("Failed to publish sensor STATE: %s", mqttClient.rc)

def waitAndLoop(mqttClient, cfg):
    global sigterm
    loopDelay = min(MAX_LOOP_DELAY, cfg['DHT_READ_DELAY'])
    logger.debug("Waiting " + str(cfg['DHT_READ_DELAY']) + " seconds with loop calls every " + str(loopDelay) + " seconds")
    for x in range(0, cfg['DHT_READ_DELAY'], loopDelay):
        if sigterm:
            break
        mqttClient.loop()
        time.sleep(loopDelay)

def waitAfterDiscovery(cfg):
    waitSeconds = cfg['HA_WAIT_AFTER_DISCOVERY']
    if waitSeconds > 0:
        logger.debug("Waiting for " + str(waitSeconds) + " seconds to let subscribers process discovery")
        time.sleep(waitSeconds)
        logger.debug("Done waiting")


##### DHT SENSOR #####

def initialiseSensor(cfg, dhtDataPin):
    # GPIO setup - use the internal pull-up resistor
    dhtDataPin.direction = digitalio.Direction.INPUT
    dhtDataPin.pull = digitalio.Pull.UP
    # DHT setup
    dhtSensor = adafruit_dht.DHT22(cfg['DHT_PIN_ADA'])
    logger.info("Sensor initialised on pin " + str(cfg['DHT_PIN_ADA']))
    return dhtSensor

def terminateSensor(dhtSensor, dhtDataPin):
    logger.debug("Terminating sensor")
    dhtSensor.exit()
    dhtDataPin.deinit()

def readSensor(dhtSensor, cfg):
    temperature = 0.0
    humidity = 0.0
    read_attempts = 1
    while read_attempts <= cfg['DHT_MAX_READ_ATTEMPTS']:
        try:
            state = {
                DEVICE_TEMPERATURE : dhtSensor.temperature,
                DEVICE_HUMIDITY : dhtSensor.humidity
            }
            logger.info("Sensor was read: " + str(state))
            return state

        except RuntimeError as error:
            logger.error('Error reading DHT sensor: ' + error.args[0])
            read_attempts += 1
            time.sleep(cfg['DHT_ATTEMPT_DELAY'])

##### Main #####

def main():
    global sigterm
    try:
        cfg = initialiseConfig()
        mqttClient = initialiseMqtt(cfg)
        if cfg['HA_SENSOR_DISCOVERY']:
            publishDiscovery(mqttClient, cfg, DEVICE_TEMPERATURE)
            publishDiscovery(mqttClient, cfg, DEVICE_HUMIDITY)
            waitAfterDiscovery(cfg)
        else:
            logger.info("Skip publishing discovery as configured")
        dhtDataPin = digitalio.DigitalInOut(cfg['DHT_PIN_ADA'])
        dhtSensor = initialiseSensor(cfg, dhtDataPin)
        if cfg['HA_SENSOR_AVAILABILITY']:
            publishAvailability(mqttClient, cfg, PAYLOAD_AVAILABLE)
        else:
            logger.info("Skip publishing availability  as configured")
        while not sigterm:
            sensorState = readSensor(dhtSensor, cfg)
            publishSensorState(sensorState, mqttClient, cfg)
            mqttClient.loop()
            waitAndLoop(mqttClient, cfg)
    except RuntimeError as error:
        logger.error(error)
    finally:
        try:
            if cfg['HA_SENSOR_AVAILABILITY']:
                publishAvailability(mqttClient, cfg, PAYLOAD_NOT_AVAILABLE)
        except RuntimeError as error:
            logger.error("Failed to publish sensors inavailability: " + str(error))
            pass
        terminateSensor(dhtSensor, dhtDataPin)
        terminateMqtt(mqttClient, cfg)

logger.info("Start")
main()
logger.info("Exit")
