import board

cfg = {
    'DHT_PIN_ADA' : board.D4, # DHT data pin, here: RPI GPIO 4
    'DHT_READ_DELAY' : 180, # Delay (seconds) between sensor readings
    'DHT_MAX_READ_ATTEMPTS' : 3, # Each reading may comprise multiple attemps to read from the sensor until the reading is skipped/aborted
    'DHT_ATTEMPT_DELAY' : 3.0, # Seconds between read attempts
    'HA_SENSOR_ID' : "DHTsensor1", # ID to create/identify the multi-valued sensor in Home Assistant (HA)
    'HA_SENSOR_ID_TEMPERATURE' : "DHTsensor1T", # ID to create/identify the sensor's temperature component in Home Assistant
    'HA_SENSOR_ID_HUMIDITY' : "DHTsensor1H", # ID to create/identify the sensor's humidity component in Home Assistant
    'HA_SENSOR_DISCOVERY' : True, # If True, send device discovery messages to Home Assistant
    'HA_WAIT_AFTER_DISCOVERY' : 7, # Delay (seconds) after sending discovery to let Home Assistant process the discovery message and create the entitites. Set to 0 to disable delay.
    'HA_SENSOR_AVAILABILITY' : True, # If True, send device availability messages to Home Assistant
    'HA_SENSOR_NAME_TEMPERATURE' : "Temperature", # Name of the temperature component to appear in Home Assistant
    'HA_SENSOR_NAME_HUMIDITY' : "Humidity", # Name of the humidity component to appear in Home Assistant
    'HA_SENSOR_EXPIRE_AFTER' : 1800, # Number of seconds after the sensor’s state expires, if it’s not updated.
    'MQTT_BROKER_HOST' : "mqtt.homeassistant.local",
    'MQTT_BROKER_PORT' : 8883,
    'MQTT_TOPIC_STATE' : "homeassistant/sensor/{0}/state", # Topic to jointly publish temperature and humidity. Placeholder is set to SENSOR_ID
    'MQTT_TOPIC_AVAILABILITY' : "homeassistant/sensor/{0}/availability", # Topic to indicate sensor's availability. Placeholder is set to SENSOR_ID
    'MQTT_TOPIC_CONFIG' : "homeassistant/sensor/{0}/config", # The sensor's temperature and the humidity each have their own config topics for discovery. Placeholder is set to HA_SENSOR_ID_TEMPERATURE and HA_SENSOR_ID_HUMIDITY
    'MQTT_USERNAME': None,
    'MQTT_PASSWORD' : None,
    'MQTT_CA_CERTS': None, # Path to CA certificate file
    'MQTT_QOS' : 1 # QoS used for all publishing (discovery, availability, state)
}
